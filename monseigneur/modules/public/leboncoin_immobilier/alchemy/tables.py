from sqlalchemy import Column
from sqlalchemy.orm import relationship, backref

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, TEXT, VARCHAR, BOOLEAN, DATETIME, LONGTEXT, FLOAT, INTEGER
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey


Base = declarative_base()


class Housing(Base):

    __tablename__ = 'housing'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}

    id = Column(INTEGER, primary_key=True, autoincrement=True)
    scraping_time = Column(DATETIME)

    housing_id = Column(VARCHAR(100), unique=True)
    first_publication_date = Column(DATETIME)
    expiration_date = Column(DATETIME)
    index_date = Column(DATETIME)
    status = Column(BOOLEAN)
    category_id = Column(INTEGER)
    category_name = Column(VARCHAR(500))
    subject = Column(VARCHAR(500))
    body = Column(MEDIUMTEXT)
    url = Column(MEDIUMTEXT)
    price = Column(INTEGER)
    region_id = Column(INTEGER)
    region_name = Column(VARCHAR(500))
    city_label = Column(VARCHAR(500))
    city = Column(VARCHAR(500))
    zip_code = Column(VARCHAR(500))  # zipcode
    lat = Column(FLOAT)
    lng = Column(FLOAT)
    owner_name = Column(VARCHAR(500))  # owner > user_id
    owner_id = Column(VARCHAR(500))  # owner > name


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)

