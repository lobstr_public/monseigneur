from monseigneur.core.browser.pages import HTMLPage, JsonPage, pagination, Page
from monseigneur.core.browser.elements import ItemElement, ListElement, method, DictElement
from monseigneur.core.browser.filters.html import Link, AbsoluteLink
from monseigneur.core.browser.filters.json import Dict
from monseigneur.core.browser.filters.standard import CleanText, Regexp, CleanDecimal, Currency, DateTime, Env, Field, Currency as CleanCurrency, CleanDate
from monseigneur.modules.public.leboncoin.alchemy.tables import Housing

import json


class ListPage(HTMLPage):

    ENCODING = 'UTF8'

    def build_doc(self, content):

        self.html_doc = HTMLPage.build_doc(self, content)

        add_content = CleanText('//script[contains(text(), "__REDIAL_PROPS__")]')(self.html_doc)

        add_content = add_content.replace('window.__REDIAL_PROPS__ =', '')
        add_content = json.loads(add_content)

        for content in add_content:
            if content != None:
                self.doc = content
                return self.doc


    @method
    class iter_housing(DictElement):
        item_xpath = "data/ads"

        class get_housing(ItemElement):
            klass = Housing

            def obj_housing_id(self):
                return Dict('list_id', default=None)(self)

            def obj_first_publication_date(self):
                return CleanDate(Dict("first_publication_date", default=None), default=None)(self)

            def obj_expiration_date(self):
                return CleanDate(Dict("expiration_date", default=None), default=None)(self)

            def obj_index_date(self):
                return CleanDate(Dict("expiration_date", default=None), default=None)(self)

            def obj_status(self):
                txt = Dict('status', default=None)(self)
                if txt:
                    if txt == 'active':
                        return True
                return False

            def obj_category_id(self):
                raw = Dict('category_id', default=None)(self)
                if raw:
                    return int(raw)
                return

            def obj_category_name(self):
                return Dict("category_name", default=None)(self)

            def obj_subject(self):
                return Dict("subject", default=None)(self)

            def obj_body(self):
                return Dict("body", default=None)(self)

            def obj_url(self):
                return Dict("url", default=None)(self)

            def obj_price(self):
                price_list = Dict("price", default=None)(self)
                if price_list:
                    return int(price_list[0])
                return None

            def obj_region_id(self):
                raw = Dict("location/region_id", default=None)(self)
                if raw:
                    return int(raw)
                return

            def obj_region_name(self):
                return Dict("location/region_name", default=None)(self)

            def obj_city_label(self):
                return Dict("location/city_label", default=None)(self)

            def obj_city(self):
                return Dict("location/city", default=None)(self)

            def obj_zip_code(self):
                return Dict("location/zipcode", default=None)(self)

            def obj_lat(self):
                lat = Dict("location/lat", default=None)(self)
                if lat:
                    return CleanDecimal().filter(lat)
                return

            def obj_lng(self):
                lng = Dict('location/lng', default=None)(self)
                if lng:
                    return CleanDecimal().filter(lng)
                return

            def obj_owner_name(self):
                return Dict('owner/name', default=None)(self)

            def obj_owner_id(self):
                return Dict('owner/user_id', default=None)(self)
