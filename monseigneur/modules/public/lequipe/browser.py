# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

from monseigneur.core.browser import PagesBrowser, URL
from .pages import ApiPage

__all__ = ['LequipeBrowser']


class LequipeBrowser(PagesBrowser):

    BASEURL = 'https://www.lequipe.fr/'

    api_page = URL("https://dwh.lequipe.fr/api/edito/chrono\?path=/&context=page&page=(?P<page>\d+)", ApiPage)

    def __init__(self, *args, **kwargs):
        super(LequipeBrowser, self).__init__(*args, **kwargs)

    def iter_articles(self, page):
        self.api_page.go(page=page)
        assert self.api_page.is_here()
        return self.page.iter_articles()
