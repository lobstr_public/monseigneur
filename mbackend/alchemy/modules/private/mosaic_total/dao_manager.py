from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_by_id(self, alchemy_object, id):
        return self.daoFactory.select_by_id(alchemy_object, id)

    def insert(self, instance_object):
        return self.daoFactory.insert(instance_object)

    def accounts_count(self, alchemy_object):
        return self.daoFactory.accounts_count(alchemy_object)

    def select_account_by_id(self, alchemy_object, id):
        return self.daoFactory.select_account_by_id(alchemy_object, id)

    def get_last_prices(self):
        return self.daoFactory.get_last_prices()
