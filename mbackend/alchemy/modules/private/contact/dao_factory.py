from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import create_all, Contact
from mbackend.alchemy.modules.private.google_maps.tables import RawResult

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noclosingandsession, dbconnect_noexpire
from pytz import timezone
import datetime
from sqlalchemy import and_, or_

from sqlalchemy.orm import scoped_session


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect_noexpire
    def select_no_scraped_people(self, session, limit=0):
        contacts = session.query(RawResult).filter(and_(RawResult.is_contact == 0)).limit(limit).all()
        return contacts

    @dbconnect
    def insert_contact(self, session, obj, ref_id):
        obj.ref_id = ref_id
        session.add(obj)

    @dbconnect
    def count_contact(self, session, obj):
        count = session.query(Contact).filter(Contact.value == obj.value).count()
        return count

    @dbconnect
    def update_people(self, session, ref_id):
        obj = session.query(RawResult).filter(RawResult.id == ref_id).one()
        obj.is_contact = 1
