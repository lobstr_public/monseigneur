from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def select_by_id(self, session, alchemy_object, instance_object, id_name):
        return self.daoFactory.select_by_id(session, alchemy_object, instance_object, id_name)

    def select_monitoring(self, session):
        return self.daoFactory.select_monitoring(session)

    def count_by_id(self, session, parent_object, child_object, id_name):
        return self.daoFactory.count_by_id(parent_object, child_object, id_name)

    def monitoring_exists(self, session, monitoring):
        return self.daoFactory.monitoring_exists(session, monitoring)
