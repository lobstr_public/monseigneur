from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT, FLOAT, BOOLEAN
from mbackend.alchemy.modules.watches.tables import Base, Auction


class ExtendedAuction(Auction):
    monitoring_id = Column(Integer, ForeignKey('monitoring.id'))


class Monitoring(Base):
    __tablename__ = "monitoring"

    id = Column(Integer, primary_key=True, autoincrement=True)
    month = Column(Integer)
    year = Column(Integer)
    last_auction_scraped = Column(Integer)
    total_auctions = Column(Integer)

    monitoring_auctions = relationship("ExtendedAuction")
