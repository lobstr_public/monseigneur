from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT, FLOAT, BOOLEAN
from sqlalchemy.types import DateTime
from mbackend.alchemy.modules.watches.tables import Base

from mbackend.alchemy.modules.watches.tables import Auction, Watch, Image, Artist, Contact


class Auction(Auction):
    page = Column(Integer)
    is_done = Column(BOOLEAN)


class Monitoring(Base):
    __tablename__ = "monitoring"

    id = Column(Integer, primary_key=True, autoincrement=True)
    total_auctions = Column(Integer)
    last_auction_scraped = Column(Integer)
    last_scraping_date = Column(DateTime)
