from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT, FLOAT, BOOLEAN, DATETIME
from mbackend.alchemy.modules.watches.tables import Base

