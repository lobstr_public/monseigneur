from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from mbackend.alchemy.modules.private.watches.tables import create_all

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy import and_, or_


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def select_by_id(self, session, alchemy_object, id_number, id_name):
        assert isinstance(id_name, str)
        assert hasattr(alchemy_object, id_name)
        return session.query(alchemy_object).filter(getattr(alchemy_object, id_name) == id_number).one()

    def count_by_id(self, session, parent_object, child_object, id_name):
        assert isinstance(id_name, str)
        assert hasattr(parent_object, id_name)
        assert hasattr(child_object, id_name)
        return session.query(parent_object).filter(getattr(parent_object, id_name) == getattr(child_object, id_name)).count()
