from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship
from sqlalchemy.types import DateTime

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT, FLOAT, BOOLEAN
from mbackend.alchemy.modules.private.watches.tables import Base, Auction


class Auction(Auction):
    auction_sessions = relationship("AuctionSession")
    page = Column(Integer)
    total_pages = Column(Integer)
    scraping_date = Column(DateTime)


class AuctionSession(Base):
    __tablename__ = "auction_session"

    id = Column(Integer, primary_key=True, autoincrement=True)
    auction_id = Column(VARCHAR(300), ForeignKey('auction.auction_id'))
    id_session = Column(Integer)
    date = Column(DateTime)
    content = Column(VARCHAR(200))
