from alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import create_all, Monitoring, Annonce

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy.sql.expression import exists
from sqlalchemy import or_
from datetime import timedelta, datetime, date


class DaoFactory(DaoFactory):

    HOURS_INTERVAL_TO_SCRAPE = 12

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    def select_by_annonce_id(self, session, alchemy_object, annonce_id):
        return session.query(alchemy_object).filter(alchemy_object.annonce_id == annonce_id).one()

    def select_all(self, session, alchemy_object):
        return session.query(alchemy_object).all()

    @dbconnect
    def get_phone_for_annonce(self, session, annonce):
        return session.query(Annonce).filter(Annonce.annonce_id == annonce.annonce_id).one()

    @dbconnect_noclosingandsession
    def select_monitoring(self, session):
        # return session.query(Monitoring).all()
        return session.query(Monitoring).one()

    def insert_annonce(self, session, annonce):
        session.add(annonce)
        session.commit()
        # object is filled with inserted primary key
        return annonce

    @dbconnect
    def annonce_exists(self, session, annonce):
        return session.query(exists().where(Annonce.annonce_id == annonce.annonce_id)).scalar()
