from sqlalchemy import Column, Integer, ForeignKey, UniqueConstraint
from sqlalchemy.types import DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import VARCHAR, BOOLEAN, JSON, FLOAT, DECIMAL
from mbackend.alchemy.core.base import Base


"""
class CookieSession(Base):

    state = Column(VARCHAR(10000))
    ip = Column(VARCHAR(100))
    session_id = Column(Integer)
    module = Column(VARCHAR(30))
    url = Column(VARCHAR(2000), index=True)
    creation_time = Column(DateTime, nullable=False)
    last_time_used = Column(DateTime, nullable=True)
    error = Column(VARCHAR(150))


class UsedSessions(Base):
    cookies_session_id  = Column(Integer, nullable=False, unique=True, index=True)
"""


class Task(Base):

    url = Column(VARCHAR(2000), index=True)
    page = Column(Integer)

    country = Column(VARCHAR(30))

    is_done = Column(BOOLEAN)

    creation_time = Column(DateTime, nullable=False)
    scraping_time = Column(DateTime, nullable=True)

    is_active = Column(BOOLEAN)

    params = Column(JSON)

    total_results = Column(Integer)
    page_elapsed = Column(FLOAT)
    total_elapsed = Column(FLOAT)
    total_pages = Column(Integer)
    results_per_page = Column(Integer)
    max_results = Column(Integer)

    slug = Column(VARCHAR(30))
    price_min = Column(VARCHAR(10))
    price_max = Column(VARCHAR(10))
    postal_code = Column(Integer)
    location = Column(VARCHAR(50))
    scraping_date = Column(DateTime)
    last_result_date = Column(DateTime)
    is_done = Column(BOOLEAN)

    result_type = Column(Integer)


class ProfileTask(Base):

    __tablename__ = 'profile_task'

    task_id = Column(Integer, ForeignKey('task.id'), primary_key=True)
    profile_id = Column(Integer, nullable=False)

    task = relationship("Task")

    status = Column(BOOLEAN)
    group_id = Column(Integer)
    done_time = Column(DateTime)
    is_done = Column(BOOLEAN)
    frequency = Column(Integer)
    has_errors = Column(BOOLEAN)
    errors = Column(JSON)
    error_date = Column(DateTime)
    max_page = Column(Integer)
    page = Column(Integer)
    creation_time = Column(DateTime, nullable=False)

    total_pages = Column(Integer)
    last_page_scraped = Column(Integer)
    is_active = Column(BOOLEAN)
    last_result_date = Column(DateTime)
    last_contact_scraped = Column(Integer)
    last_result_scraped = Column(Integer)
    total_results = Column(Integer)

    module_name = Column(VARCHAR(100))


class Schedule(Base):

    name = Column(VARCHAR(300))
    is_active = Column(BOOLEAN)
    comment = Column(VARCHAR(2000))
    cron_expression = Column(VARCHAR(100))
    profile_task_id = Column(Integer, ForeignKey('profile_task.id'), primary_key=True)
    # task_id = Column(Integer, ForeignKey('task.id'), primary_key=True)

    __table_args__ = (UniqueConstraint('profile_task_id', 'name', name='_unique_tasks_id_name'),)
    __table_args__ = (UniqueConstraint('profile_task_id', 'cron_expression', name='_unique_tasks_id_cron_expression'),)


class Run(Base):

    __tablename__ = 'run'

    profile_task_id = Column(Integer, ForeignKey('profile_task.id'), primary_key=True)
    start_date = Column(DateTime)
    end_date = Column(DateTime)
    duration = Column(DECIMAL)
    origin = Column(VARCHAR(100))


class SubTask(Base):

    page = Column(Integer)
    done_time = Column(DateTime)
    total_results = Column(Integer)

    url = Column(VARCHAR(10000))
    next_url = Column(VARCHAR(10000))
    previous_url = Column(VARCHAR(10000))

    task_id = Column(Integer, ForeignKey('task.id'))


class SubTaskResult(Base):

    __tablename__ = 'subtask_result'

    subtask_id = Column(Integer, nullable=False)
    result_id = Column(Integer, nullable=False)

    association_time = Column(DateTime)
    last_scraping_time = Column(DateTime)


class Request(Base):

    __table_args__ = {'extend_existing': True}

    url = Column(VARCHAR(2000), unique=False)
    scraping_time = Column(DateTime, nullable=True)
    request_type = Column(VARCHAR(100))

    task_id = Column(Integer, ForeignKey('task.id'))
    profile_id = Column(Integer, ForeignKey('profile.id'), nullable=False)


class Ban(Base):

    __table_args__ = {'extend_existing': True}

    url = Column(VARCHAR(2000), unique=False)
    browser = Column(VARCHAR(20000))
    scraping_time = Column(DateTime, nullable=True)
    ban_type = Column(VARCHAR(100))
    ban_count = Column(Integer)

    task_id = Column(Integer, ForeignKey('task.id'))
    profile_id = Column(Integer, ForeignKey('profile.id'), nullable=False)
