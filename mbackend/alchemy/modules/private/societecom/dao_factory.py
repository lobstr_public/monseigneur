from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import create_all

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy import and_


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect_noclosingandsession
    def select_by_id(self, session, alchemy_object, id):
        return session.query(alchemy_object).filter(alchemy_object.id == id).one()

    @dbconnect  # decorator to handle SQLAlchemy exception... can deep dive...
    # creating function to count if company already in base
    def count_by_siret(self, session, alchemy_object, company_object):
        return session.query(alchemy_object).filter(alchemy_object.siret == company_object.siret).count()

    @dbconnect
    def count_by_nom_and_ape(self, session, alchemy_object, nom, ape):
        return session.query(alchemy_object).filter(and_(alchemy_object.nom == nom, ape == alchemy_object.code_ape_de_lentreprise)).count()
