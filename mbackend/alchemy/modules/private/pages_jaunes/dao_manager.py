from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_by_id(self, alchemy_object, id):
        return self.daoFactory.select_by_id(alchemy_object, id)

    def select_last_page_scraped(self, alchemy_object, location, keyword):
        return self.daoFactory.select_last_page_scraped(alchemy_object, location, keyword)

    def count_monitoring(self, alchemy_object, location, keyword):
        return self.daoFactory.count_monitoring(alchemy_object, location, keyword)

    def count_monitoring_is_done(self, alchemy_object, location, keyword):
        return self.daoFactory.count_monitoring_is_done(alchemy_object, location, keyword)

    def update_monitoring(self, alchemy_object, keyword, location, page):
        return self.daoFactory.update_monitoring(alchemy_object, keyword, location, page)

    def update_monitoring_total_pages(self, alchemy_object, keyword, location, total_pages):
        return self.daoFactory.update_monitoring_total_pages(alchemy_object, keyword, location, total_pages)

    def count_item(self, alchemy_object, item_id):
        return self.daoFactory.count_item(alchemy_object, item_id)

    def insert(self, alchemy_instance):
        return self.daoFactory.insert(alchemy_instance)

    def update_item(self, alchemy_instance):
        return self.daoFactory.update_item(alchemy_instance)
