from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import create_all
from .tables import Item

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone
import datetime
from sqlalchemy import and_, or_

from sqlalchemy.orm import scoped_session


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect_noclosingandsession
    def select_by_id(self, session, alchemy_object, id):
        return session.query(alchemy_object).filter(alchemy_object.id == id).one()

    @dbconnect
    def count_monitoring(self, session, alchemy_object, location, keyword):
        return session.query(alchemy_object).filter(and_(
            alchemy_object.location == location, alchemy_object.keyword == keyword)).count()

    @dbconnect
    def count_monitoring_is_done(self, session, alchemy_object, location, keyword):
        return session.query(alchemy_object).filter(
            and_(alchemy_object.location == location, alchemy_object.keyword == keyword,
                 alchemy_object.is_done == 1)).count()

    @dbconnect
    def select_last_page_scraped(self, session, alchemy_object, location, keyword):
        obj = session.query(alchemy_object).filter(
            and_(alchemy_object.location == location, alchemy_object.keyword == keyword)).one()
        return obj.last_page_scraped

    @dbconnect
    def update_monitoring(self, session, alchemy_object, keyword, location, page):
        obj = session.query(alchemy_object).filter(
            and_(alchemy_object.keyword == keyword, alchemy_object.location == location)).one()
        obj.last_page_scraped = page
        if obj.last_page_scraped == obj.total_pages:
            obj.is_done = 1
        obj.scraping_time = datetime.datetime.now()
        session.commit()

    @dbconnect
    def update_monitoring_total_pages(self, session, alchemy_object, keyword, location, total_pages):
        obj = session.query(alchemy_object).filter(
            and_(alchemy_object.keyword == keyword, alchemy_object.location == location)).one()
        obj.total_pages = total_pages
        session.commit()

    @dbconnect
    def count_item(self, session, alchemy_object, item_id):
        return session.query(alchemy_object).filter(alchemy_object.item_id == item_id).count()

    @dbconnect
    def insert(self, session, alchemy_instance):
        return session.add(alchemy_instance)

    @dbconnect
    def update_item(self, session, alchemy_instance):
        assert alchemy_instance.item_id
        obj_from_table = session.query(Item).filter(Item.item_id == alchemy_instance.item_id).first()
        obj_from_table.cp = alchemy_instance.cp
        obj_from_table.scraping_time = datetime.datetime.now()
