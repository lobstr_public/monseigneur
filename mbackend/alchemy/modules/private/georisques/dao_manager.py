from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory
from .tables import *

class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_by_id(self, alchemy_object, id):
        return self.daoFactory.select_by_id(alchemy_object, id)

    def count_task(self, unique_task_id):
        return self.daoFactory.count_task(unique_task_id)

    def count_if_task_already_done(self, address, request_time):
        return self.daoFactory.count_if_task_already_done(address, request_time)

    def add_task(self, address, request_time, unique_task_id):
        return self.daoFactory.add_task(address, request_time, unique_task_id)

    def update_task_as_done(self, unique_task_id, base64_pdf, time_elapsed, scraping_time, path):
        return self.daoFactory.update_task_as_done(unique_task_id, base64_pdf, time_elapsed, scraping_time, path)

    def update_task_as_empty(self, unique_task_id):
        return self.daoFactory.update_task_as_done(unique_task_id)

    def update_task_as_error(self, unique_task_id, backtrace):
        return self.daoFactory.update_task_as_error(unique_task_id, backtrace)

    def select_object_done(self, address):
        return self.daoFactory.select_object_done(address)


