from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import *

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy import and_

import datetime

from slugify import slugify


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect_noclosingandsession
    def select_by_id(self, session, alchemy_object, id):
        return session.query(alchemy_object).filter(alchemy_object.id == id).one()

    @dbconnect
    def count_task(self, session, unique_task_id):
        return session.query(Task).filter(Task.unique_task_id == unique_task_id).count()

    @dbconnect
    def count_if_task_already_done(self, session, address, request_time):
        task_object = session.query(Task).filter(and_(Task.addr_string == address, Task.status == 1)).order_by(Task.id.desc()).first()
        if task_object:
            if task_object.addr_string == address and request_time < task_object.creation_time + datetime.timedelta(minutes=60):
                return True
        return False

    @dbconnect
    def add_task(self, session, address, request_time, unique_task_id):
        task_object = Task()
        task_object.addr_string = address
        task_object.creation_time = request_time
        task_object.unique_task_id = unique_task_id
        session.add(task_object)

    @dbconnect
    def update_task_as_done(self, session, unique_task_id, base64_pdf, time_elapsed, scraping_date, path):
        task_object = session.query(Task).filter(Task.unique_task_id == unique_task_id).one()
        task_object.base64_pdf = base64_pdf
        task_object.time_elapsed = time_elapsed
        task_object.scraping_date = scraping_date
        task_object.path = path
        task_object.status = 1

    @dbconnect
    def update_task_as_empty(self, session, unique_task_id):
        task_object = session.query(Task).filter(Task.unique_task_id == unique_task_id).one()
        task_object.base64_pdf = base64_pdf
        task_object.time_elapsed = time_elapsed
        task_object.scraping_time = scraping_time
        task_object.status = 1

    @dbconnect
    def update_task_as_error(self, session, unique_task_id, backtrace):
        task_object = session.query(Task).filter(Task.unique_task_id == unique_task_id).one()
        task_object.backtrace = backtrace
        task_object.scraping_time = datetime.datetime.utcnow()
        task_object.status = -1

    @dbconnect_noexpire
    def select_object_done(self, session, address):
        task_object = session.query(Task).filter(and_(Task.addr_string == address, Task.status == 1)).order_by(Task.id.desc()).first()
        return task_object
