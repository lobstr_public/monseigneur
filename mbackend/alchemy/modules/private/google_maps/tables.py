from sqlalchemy import Column, Integer

from sqlalchemy.dialects.mysql import MEDIUMTEXT, LONGTEXT, VARCHAR, DATETIME, FLOAT, JSON, TINYINT, BOOLEAN
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class ApiResult(Base):
    """Result object for sqlalchemy-driven db"""

    __tablename__ = 'google_maps_api'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    mysql_charset = 'utf8mb4'

    id = Column(Integer, primary_key=True, autoincrement=True)
    place_id = Column(VARCHAR(500), unique=True)
    scraping_time = Column(DATETIME)

    keyword = Column(VARCHAR(500))
    name = Column(MEDIUMTEXT)
    icon = Column(MEDIUMTEXT)
    types = Column(MEDIUMTEXT)
    address = Column(MEDIUMTEXT)
    lat = Column(FLOAT)
    lng = Column(FLOAT)
    vicinity = Column(MEDIUMTEXT)
    reviews = Column(Integer)
    score = Column(FLOAT)

    phone = Column(MEDIUMTEXT)
    international_phone = Column(MEDIUMTEXT)

    monday_hours = Column(MEDIUMTEXT)
    tuesday_hours = Column(MEDIUMTEXT)
    wednesday_hours = Column(MEDIUMTEXT)
    thursday_hours = Column(MEDIUMTEXT)
    friday_hours = Column(MEDIUMTEXT)
    saturday_hours = Column(MEDIUMTEXT)
    sunday_hours = Column(MEDIUMTEXT)

    json = Column(JSON)


class RawResult(Base):
    """Result object for sqlalchemy-driven db"""

    __tablename__ = 'google_maps_raw'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    mysql_charset = 'utf8mb4'

    id = Column(Integer, primary_key=True, autoincrement=True)
    scraping_time = Column(DATETIME)

    keyword = Column(MEDIUMTEXT)
    target = Column(MEDIUMTEXT)

    name = Column(MEDIUMTEXT)
    address = Column(MEDIUMTEXT)
    coordinates = Column(MEDIUMTEXT)
    url = Column(MEDIUMTEXT)
    zero_x = Column(MEDIUMTEXT)

    reviews = Column(Integer)
    score = Column(FLOAT)
    phone = Column(MEDIUMTEXT)
    website = Column(MEDIUMTEXT)
    type = Column(MEDIUMTEXT)

    raw = Column(LONGTEXT)

    is_contact = Column(BOOLEAN, default=0)


class Monitoring(Base):
    """Result object for sqlalchemy-driven db"""

    __tablename__ = 'monitoring'
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    mysql_charset = 'utf8mb4'

    id = Column(Integer, primary_key=True, autoincrement=True)

    location = Column(MEDIUMTEXT)
    keyword = Column(MEDIUMTEXT)

    is_done = Column(TINYINT(1), default=0)

def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
