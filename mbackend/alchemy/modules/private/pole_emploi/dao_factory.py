from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import create_all, Profile, Monitoring

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy.sql.expression import exists
from sqlalchemy import or_, and_
from datetime import timedelta, datetime, date


class DaoFactory(DaoFactory):

    HOURS_INTERVAL_TO_SCRAPE = 12

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    def select_by_annonce_id(self, session, alchemy_object, annonce_id):
        return session.query(alchemy_object).filter(alchemy_object.annonce_id == annonce_id).one()

    @dbconnect
    def count_by_unique_id(self, session, _id):
        return session.query(Profile).filter(Profile.id_candidat == _id).count()

    @dbconnect
    def insert(self, session, _object):
        return session.add(_object)

    @dbconnect
    def count_monitoring_exist(self, session, postal_code, job_code, skill_code):
        return session.query(Monitoring).filter(
            and_(Monitoring.code_postal == postal_code, Monitoring.code_job == job_code,
                 Monitoring.code_competence == skill_code)).count()

    @dbconnect
    def count_monitoring_is_done(self, session, postal_code, job_code, skill_code):
        return session.query(Monitoring).filter(
            and_(Monitoring.code_postal == postal_code, Monitoring.code_job == job_code,
                 Monitoring.code_competence == skill_code, Monitoring.is_done == 1)).count()

    @dbconnect
    def update_monitoring_is_done(self, session, postal_code, job_code, skill_code, total_results):
        _object = session.query(Monitoring).filter(
            and_(Monitoring.code_job == job_code, Monitoring.code_postal == postal_code,
                 Monitoring.code_competence == skill_code, Monitoring.is_done == 0)).one()
        _object.total_results = total_results
        _object.is_done = 1
        _object.scraping_time = datetime.now()

    @dbconnect
    def count_total_results(self, session, postal_code, job_code, skill_code):
        return session.query(Profile).filter(and_(Profile.code_postal == postal_code, Profile.code_job == job_code,
                                                  Profile.code_competence == skill_code)).count()
