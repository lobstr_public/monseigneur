from sqlalchemy import Column
from sqlalchemy.types import DateTime
from sqlalchemy.dialects.mysql import VARCHAR, JSON
from mbackend.alchemy.core.base import Base


class CookieSession(Base):

    module_name = Column(VARCHAR(30))
    state = Column(VARCHAR(10000))
    luminati_ip = Column(VARCHAR(100))
    proxy = Column(VARCHAR(200), unique=True)
    useragent = Column(VARCHAR(200))
    creation_time = Column(DateTime, nullable=False)
    last_time_used = Column(DateTime, nullable=True)
    error = Column(VARCHAR(150))


class UsedSessions(Base):
    session_list = Column(JSON)
