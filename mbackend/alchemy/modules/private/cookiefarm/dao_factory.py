from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory
from .tables import Base


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config):
        super(DaoFactory, self).__init__(database_name, engine_config)
        # Base.create_all(self.engine)

    def get_cookie_session(self):
        pass

    def get_existing_cookie_session(self):
        pass

    def insert_cookie_session(self):
        pass
