from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_by_id(self, alchemy_object, id):
        return self.daoFactory.select_by_id(alchemy_object, id)

    def insert(self, instance_object):
        return self.daoFactory.insert(instance_object)

    # Part 1 methods
    def count_category_url(self, alchemy_object, category_url):
        return self.daoFactory.count_category_url(alchemy_object, category_url)

    def count_product_url(self, alchemy_object, product_url):
        return self.daoFactory.count_product_url(alchemy_object, product_url)

    # Part 2 methods
    def get_unscraped_products_count(self, alchemy_object):
        return self.daoFactory.get_unscraped_products_count(alchemy_object)

    def get_unscraped_products_list(self, alchemy_object):
        return self.daoFactory.get_unscraped_products_list(alchemy_object)

    def product_count(self, alchemy_object, product_url):
        return self.daoFactory.product_count(alchemy_object, product_url)

    def set_as_scraped(self, alchemy_object, product_url):
        return self.daoFactory.set_as_scraped(alchemy_object, product_url)

    def get_product_id(self, alchemy_object, unique_value):
        return self.daoFactory.get_product_id(alchemy_object, unique_value)

    def get_id_through_link(self, alchemy_object, link):
        return self.daoFactory.get_id_through_link(alchemy_object, link)

    def remove_broken_link(self, alchemy_object, id):
        return self.daoFactory.remove_broken_link(alchemy_object, id)
