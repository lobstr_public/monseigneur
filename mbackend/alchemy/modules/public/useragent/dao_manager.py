from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_by_id(self, alchemy_object, id):
        return self.daoFactory.select_by_id(alchemy_object, id)

    def select_not_done(self, alchemy_object):
        return self.daoFactory.select_not_done(alchemy_object)

    def count_by_keyword(self, alchemy_object, keyword):
        return self.daoFactory.count_by_keyword(alchemy_object, keyword)

    def select_by__id(self, alchemy_object, _id):
        return self.daoFactory.select_by__id(alchemy_object, _id)

    def update_by__id(self, alchemy_object, filled_object, keyword):
        return self.daoFactory.update_by__id(alchemy_object, filled_object, keyword)
