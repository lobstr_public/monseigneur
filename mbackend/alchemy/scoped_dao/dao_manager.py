class DaoManager:

    def get_shared_session(self):
        return self.dao_factory.get_shared_session()

    def insert(self, session, instance):
        return self.dao_factory.insert(session, instance)

    def get_if_exists(self, session, klass, instance, id_name):
        return self.dao_factory.get_if_exists(session, klass, instance, id_name)

    def object_exists(self, session, klass, instance, id_name):
        return self.dao_factory.object_exists(session, klass, instance, id_name)

    def select_by_id(self, session, klass, id_name, id_number):
        return self.dao_factory.select_by_id(session, klass, id_name, id_number)

    def select_multiple_by_id(self, session, klass, id_name, id_number):
        return self.dao_factory.select_multiple_by_id(session, klass, id_name, id_number)

    def select_by_ids(self, session, klass, id_names, id_numbers):
        return self.dao_factory.select_by_ids(session, klass, id_names, id_numbers)

    def select_multiple_by_id(self, session, klass, id_names, id_numbers):
        return self.dao_factory.select_multiple_by_id(session, klass, id_names, id_numbers)

    def select_multiple_by_ids(self, session, klass, id_names, id_numbers):
        return self.dao_factory.select_multiple_by_ids(session, klass, id_names, id_numbers)

    def count_by_id(self, session, klass, instance, id_name):
        return self.dao_factory.count_by_id(session, klass, instance, id_name)

    def count_by_ids(self, session, klass, id_names, id_numbers):
        return self.dao_factory.count_by_ids(session, klass, id_names, id_numbers)

    def bulk_save_objects(self, object_list, session=None):
        return self.dao_factory.bulk_save_objects(object_list, session=session)

    def count_by_id_noobject(self, session, klass, id_name, id_value):
        return self.dao_factory.count_by_id_noobject(session, klass, id_name, id_value)

    def add_all(self, object_list, session=None):
        return self.dao_factory.add_all(object_list, session=session)
