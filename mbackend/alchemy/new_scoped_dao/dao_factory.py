from sqlalchemy import exists
from sqlalchemy.orm.exc import NoResultFound


class DaoFactory:

    @classmethod
    def getInstance(cls, database_name, engine_config):
        new_instance = cls(database_name, engine_config)
        return new_instance

    @classmethod
    def insert(cls, session, instance):
        session.add(instance)
        # object is filled with inserted primary key
        return instance

    @classmethod
    def get_if_exists(cls, session, klass, instance, id_name):
        existing_instance = session.query(klass).filter(getattr(klass, id_name) == getattr(instance, id_name)).first()
        if existing_instance:
            return existing_instance, True
        return instance, False

    @classmethod
    def object_exists(cls, session, klass, instance, id_name):
        return session.query(exists().where(getattr(klass, id_name) == getattr(instance, id_name))).scalar()

    @classmethod
    def select_by_id(cls, session, klass, id_name, id_number):
        assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(getattr(klass, id_name) == id_number).one()
        except NoResultFound:
            session.rollback()
            return None

    @classmethod
    def select_by_ids(cls, session, klass, id_names, id_numbers):
        assert isinstance(id_names, list)
        assert isinstance(id_numbers, list)
        assert len(id_names) == len(id_numbers)
        for id_name in id_names:
            assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(*[getattr(klass, id_name) == id_number for (id_name, id_number) in zip(id_names, id_numbers)]).one()
        except NoResultFound:
            return None

    @classmethod
    def select_multiple_by_ids(cls, session, klass, id_names, id_numbers):
        assert isinstance(id_names, list)
        assert isinstance(id_numbers, list)
        assert len(id_names) == len(id_numbers)
        for id_name in id_names:
            assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(*[getattr(klass, id_name) == id_number for (id_name, id_number) in zip(id_names, id_numbers)]).all()
        except NoResultFound:
            return None

    @classmethod
    def select_multiple_by_id(cls, session, klass, id_name, id_number):
        assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(getattr(klass, id_name) == id_number).all()
        except NoResultFound:
            session.rollback()
            return None

    @classmethod
    def count_by_id(cls, session, klass, id_name, id_value):
        assert isinstance(id_name, str)
        assert hasattr(klass, id_name)
        return session.query(klass).filter(getattr(klass, id_name) == id_value).count()

    @classmethod
    def count_by_ids(cls, session, klass, id_names, id_numbers):
        assert isinstance(id_names, list)
        assert isinstance(id_numbers, list)
        assert len(id_names) == len(id_numbers)
        for id_name in id_names:
            assert hasattr(klass, id_name)
        try:
            return session.query(klass).filter(*[getattr(klass, id_name) == id_number for (id_name, id_number) in zip(id_names,id_numbers)]).count()
        except NoResultFound:
            return None

    # to bullshit to delete asap
    @classmethod
    def count_by_id_noobject(cls, session, parent_object, id_name, id_value):
        assert isinstance(id_name, str)
        assert isinstance(id_value, str)
        assert hasattr(parent_object, id_name)
        return session.query(parent_object).filter(getattr(parent_object, id_name) == id_value).count()

    @classmethod
    def bulk_save_objects(cls, object_list, session):
        session.bulk_save_objects(object_list)

    @classmethod
    def add_all(cls, session, object_list):
        session.add_all(object_list)
