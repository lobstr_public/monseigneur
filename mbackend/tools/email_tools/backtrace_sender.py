from datetime import datetime
from pytz import timezone

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class backtrace_sender():

    def __init__(self, config):

        self.config = config


        self.emailfrom = self.config["backtrace_sender"]["username"]
        self.emailto = self.config["backtrace_sender"]["username"]

        self.username = self.config["backtrace_sender"]["username"]
        self.password = self.config["backtrace_sender"]["password"]

        self.timezone = timezone('Europe/Paris')

    def send_backtrace(self, backtrace, appname, owner):

        start = datetime.now(self.timezone)
        print('START SENDING MAIL: {}'.format(str(start)))

        now = datetime.now(self.timezone)
        self.subject = "Backtrace : appname: {},  owner: {},  date {}".format(appname, owner, now)

        msg = MIMEMultipart()
        msg["From"] = self.emailfrom
        if isinstance(self.emailto, str):
            msg["To"] = self.emailto
        elif isinstance(self.emailto, list):
            msg["To"] = ', '.join(self.emailto)
        msg["Subject"] = self.subject

        text = backtrace

        mime_text = MIMEText(text, 'plain')
        msg.attach(mime_text)

        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login(self.username, self.password)
        server.sendmail(self.emailfrom, self.emailto, msg.as_string())
        server.quit()

        end = datetime.now(self.timezone)
        print('END SENDING MAIL: {}'.format(str(end)))
        print('TIME ELAPSED: {}'.format(str(end-start)))
