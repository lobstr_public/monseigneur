import requests
import datetime
from pytz import timezone


def send_complex_message(
        to_recipients_list,
        file_name,
        file_path,
        module,
        cc_recipients_list=None,
        bcc_recipients_list=["sasha.bouloudnine@lobstr.io", "simon.rochwerg@lobstr.io"],
        subject='Data delivery from your (fav) data provider')\
    :
    """

    :param to_recipients_list: list of recipients email addresses
    :param file_name: name of the file (optional)
    :param file_path: path of the file (optional)
    :param module: name of the module (to fill the text)
    :param cc_recipients_list:
    :param bcc_recipients_list: by default, lobstr.io email addresses founders
    :param subject: subject of the mail
    :return:
    """

    if all([file_path, file_name]):
        files = [("attachment", (file_name, open(file_path, "rb").read()))]
    else:
        files = []

    current_date = datetime.datetime.now(tz=timezone('Europe/Paris')).strftime('%d/%m/%Y %H:%M:%S')
    _text="Dear lobstr.io User, " \
         "\n" \
         "\n" \
         "Please, find attached your expected extraction." \
         "\n" \
         "\n" \
         "Data: {0}" \
         "\nDate: {1}" \
         "\n" \
         "\nBest," \
         "\nlobstr.io Team".format(
        module,
        current_date
    )
    response = requests.post(
        "https://api.eu.mailgun.net/v3/mail.lobstr.io/messages",
        auth=("api", "6d4e06477371f6b0ce8be932de188559-52b6835e-bb440f0e"),
        files=files,
        data={"from": "lobstr.io <notifications@mail.lobstr.io>",
              "to": to_recipients_list,
              "cc": cc_recipients_list,
              "bcc": bcc_recipients_list,
              "subject": subject,
              "text": _text
              }
    )
    print(response.text)
    print(response.status_code)

    return response


if __name__ == '__main__':
    to_recipients_list = ["sasha.bouloudnine@lobstr.io"]
    file_name = None
    file_path = None
    module = "test"
    send_complex_message(to_recipients_list=to_recipients_list, file_name=file_name, file_path=file_path, module=module)
