import os
import glob

import schedule
import time
import logging
import coloredlogs, logging

logger = logging.getLogger(__name__)
coloredlogs.install(level='DEBUG')


def clean_logs():
    logger.info('Starting cleaning')
    start = time.perf_counter()
    app_logs_dir_path = "/home/sasha/app_logs/saved_responses"
    conn_dirs = [f for f in os.listdir(app_logs_dir_path)]
    for conn_dir in conn_dirs:

        files_conn_path = '{app_logs_dir_path}/{conn_dir}/*'.format(
            app_logs_dir_path=app_logs_dir_path,
            conn_dir=conn_dir
        )
        for _file in glob.glob(files_conn_path):
            if os.path.exists(_file):
                os.remove(_file)
                logger_string = "Removed file %s" % _file
                logger.info(logger_string)
    logger.info('Cleaning done')
    logger.info('Total elapsed: %ss' % (time.perf_counter()-start))


logger.info('Starting schedule')
schedule.every(45).minutes.do(clean_logs)
logger.info('Schedule done')

while True:
    schedule.run_pending()
    time.sleep(1)
