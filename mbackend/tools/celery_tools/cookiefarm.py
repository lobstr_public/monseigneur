from mbackend.alchemy.modules.private.cookiefarm.tables import CookieSession
from mbackend.alchemy.modules.private.cookiefarm.dao_manager import DaoManager
from mbackend.alchemy.modules.private.cookiefarm.tables import CookieSession
from sqlalchemy.orm.exc import NoResultFound
import requests
from datetime import datetime


class CookieFarm():

    def __init__(self, module_name):
        self.module_name = module_name
        self.engine_config = "postgresql://postgres:AltiuS2010!?@localhost:5432/cookiefarm"
        self.dao = DaoManager("cookiefarm", engine_config=self.engine_config)
        # store =

    def load_state(self, proxy, luminati_ip):
        session, scoped_session = self.dao.get_shared_session()
        if luminati_ip:
            try:
                return session.query(CookieSession).filter(CookieSession.module_name == self.module_name, CookieSession.luminati_ip == luminati_ip).one()
            except NoResultFound:
                return None
        else:
            try:
                return session.query(CookieSession).filter(CookieSession.module_name == self.module_name, CookieSession.proxy == str(proxy)).one()
            except NoResultFound:
                return None

    def insert_state(self, state, proxy, luminati_ip, useragent):
        session, scoped_session = self.dao.get_shared_session()
        assert proxy
        assert useragent
        existing_cookiefarm_session = None
        try:
            existing_cookiefarm_session = session.query(CookieSession).filter(CookieSession.module_name == self.module_name, CookieSession.luminati_ip == luminati_ip).one()
        except NoResultFound:
            pass
        else:
            existing_cookiefarm_session.state = state
            return
        assert existing_cookiefarm_session is None
        cookiefarm_session = CookieSession()
        cookiefarm_session.state = str(state)
        cookiefarm_session.proxy = str(proxy)
        cookiefarm_session.useragent = str(useragent)
        cookiefarm_session.luminati_ip = luminati_ip
        cookiefarm_session.module_name = self.module_name
        cookiefarm_session.creation_time = datetime.now()
        session.add(cookiefarm_session)
        session.commit()
        # data = {}
        # data["state"] = state
        # data["proxy"] = proxy
        # data["module_name"] = self.module_name
        # headers = {"Content-Type": "application/json"}
        # x = requests.post("http://127.0.0.1:5000/insert_state", json=data, headers=headers)

    def update_state(self, state, state_id):
        session, scoped_session = self.dao.get_shared_session()
        pass

    def release_state(self):
        session, scoped_session = self.dao.get_shared_session()
        pass

#cookie_farm = CookieFarm("lol")
#cookie_farm.insert_state("lol")
