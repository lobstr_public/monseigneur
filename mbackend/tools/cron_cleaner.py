from threading import Thread
from pytz import timezone
import time
import os
import operator
import shutil
from datetime import datetime

from os.path import isdir, isfile, getmtime, join, getsize
from os import listdir, remove
from shutil import rmtree

from monseigneur.core.tools.log import getLogger


class CronCleaner(Thread):
    def __init__(self, path, size_option=True, time_option=False):
        Thread.__init__(self)
        self._logger = getLogger('CronCleaner')
        self._logger.warning('loool')
        input('lol')
        self._logger.info('starting CronCleaner')

        assert time_option or size_option is True
        assert time_option is not size_option

        self.tz = timezone('Europe/Paris')
        self.hour_limit = 2  # if last modification time > self.hour_limit
        self.pause_time = 3600  # time in seconds between two launching of the deletion
        self.size_limit = 5e9
        self.my_path = path

        self.size_option = size_option
        self.time_option = time_option

        assert os.path.isdir(self.my_path)

    def run(self):
        while True:
            try:
                self._logger.info('now destroying')
                if self.size_option:
                    self.clean_size()
                if self.time_option:
                    self.clean_time()

                self._logger.info('done')
                time.sleep(self.pause_time)
            except Exception as e:
                raise e

    def clean_size(self):
        list_dir = [join(self.my_path, d) for d in listdir(self.my_path) if isdir(join(self.my_path, d))]
        global_files = []
        for dir in list_dir:
            files = [join(dir, f) for f in listdir(dir)]
            for file in files:
                assert isfile(file)
                global_files.append([file, getmtime(file)])

        total_size = sum([getsize(l[0]) for l in global_files])
        if total_size > self.size_limit:
            global_files_sorted = sorted(global_files, key=operator.itemgetter(1))
            for l in global_files_sorted[:int(len(global_files_sorted) / 2)]:
                assert isfile(l[0])
                remove(l[0])
                self._logger.info('destroyed :o {}'.format(l[0]))

    def clean_time(self):
        list_dir = [join(self.my_path, d) for d in listdir(self.my_path) if isdir(join(self.my_path, d))]
        for dir in list_dir:
            time_delta = int(time.time() - getmtime(dir))
            if time_delta > 3600 * self.hour_limit:
                try:
                    shutil.rmtree(dir)
                    self._logger.warning('destroyed :o {}'.format(dir))
                except OSError:
                    time.sleep(60)
                    pass


if __name__ == '__main__':
    cron_cleaner = CronCleaner(path='/path/to/dir', size_option=True)
    cron_cleaner.start()
