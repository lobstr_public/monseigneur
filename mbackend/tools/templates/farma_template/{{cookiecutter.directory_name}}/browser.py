from monseigneur.core.browser import PagesBrowser, URL

__all__ = ['{{cookiecutter.camel_name}}Browser']


class {{cookiecutter.camel_name}}Browser(PagesBrowser):
    BASEURL = ''

    def __init__(self, *args, **kwargs):
        super({{cookiecutter.camel_name}}Browser, self).__init__(*args, **kwargs)

    # iter_subdomains used for collecting multisite subdomains
    # def iter_subdomains(self, url=None, zip_code=None):
    #     pass

    def iter_meta(self):
        pass

    def get_meta(self, meta_obj):
        pass

    def iter_categories(self):
        pass

    def go_subcategory(self, category_obj, page):
        pass

    def get_total_products(self):
        pass

    def get_total_pages(self):
        pass

    def iter_products(self):
        pass

    def fill_product_and_history_details(self, product_obj, history_obj):
        pass

    # def iter_feedbacks(self, product_obj):
    #     pass

    # def iter_child_products(self, product_obj, history_obj):
    #     pass

    # def fill_child_product_and_history_details(self, child_product_obj, child_history_obj):
    #     pass
