from monseigneur.core.tools.backend import Module
from .browser import {{cookiecutter.camel_name}}Browser

__all__ = ['{{cookiecutter.camel_name}}Module']


class {{cookiecutter.camel_name}}Module(Module):
    NAME = '{{cookiecutter.camel_name}}'
    BROWSER = {{cookiecutter.camel_name}}Browser

    def iter_meta(self):
        return self.browser.iter_meta()

    def get_meta(self, meta_obj):
        return self.browser.get_meta(meta_obj)

    # iter_subdomains used for collecting multisite subdomains
    # def iter_subdomains(self, url=None, zip_code=None):
    #     return self.browser.iter_subdomains(url, zip_code)

    def iter_categories(self):
        return self.browser.iter_categories()

    def go_subcategory(self, category_obj, page):
        return self.browser.go_subcategory(category_obj, page)

    def get_total_pages(self):
        return self.browser.get_total_pages()

    def get_total_products(self):
        return self.browser.get_total_products()

    def iter_products(self, *args, **kwargs):
        return self.browser.iter_products()

    def fill_product_and_history_details(self, product_obj, history_obj):
        return self.browser.fill_product_and_history_details(product_obj, history_obj)

    # def has_feedbacks(self):
    #     return True

    # def iter_feedbacks(self, product_obj, *args, **kwargs):
    #     return self.browser.iter_feedbacks(product_obj)

    # def has_child_products(self):
    #     return True

    # def iter_child_products(self, product_obj, *arg, **kwargs):
    #     return self.browser.iter_child_products(product_obj)

    # def fill_child_product_and_history_details(self, child_product_obj, child_history_obj):
    #     return self.browser.fill_child_product_and_history_details(child_product_obj, child_history_obj)
