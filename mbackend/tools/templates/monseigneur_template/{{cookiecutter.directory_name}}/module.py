from monseigneur.core.tools.backend import Module
from .browser import {{cookiecutter.camel_name}}Browser

__all__ = ['{{cookiecutter.camel_name}}Module']


class {{cookiecutter.camel_name}}Module(Module):
    NAME = '{{cookiecutter.camel_name}}'
    BROWSER = {{cookiecutter.camel_name}}Browser
