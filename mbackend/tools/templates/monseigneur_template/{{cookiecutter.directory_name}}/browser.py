from monseigneur.core.browser import PagesBrowser, URL

__all__ = ['{{cookiecutter.camel_name}}Browser']


class {{cookiecutter.camel_name}}Browser(PagesBrowser):
    # BASEURL = base URL of site to be scraped
    # BASEURL = ''

    def __init__(self, *args, **kwargs):
        super({{cookiecutter.camel_name}}Browser, self).__init__(*args, **kwargs)
