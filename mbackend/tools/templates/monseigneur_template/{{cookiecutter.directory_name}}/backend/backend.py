from mbackend.core.fetcher import Fetcher
from mbackend.core.application import Application
from monseigneur.modules.team.{{cookiecutter.directory_name}}.alchemy.dao_manager import DaoManager

import pytz


class {{cookiecutter.camel_name}}Backend(Application):
    APPNAME = "Application {{cookiecutter.camel_name}}"
    VERSION = "1.0"

    def __init__(self):
        self.setup_logging()

        self.module_name = '{{cookiecutter.directory_name}}'

        super({{cookiecutter.camel_name}}Backend, self).__init__(self.APPNAME)
        # custom_path is the directory above
        # ~/mdev/monseigneur/monseigneur/modules
        # custom_path = ''
        self.fetcher = Fetcher(custom_path=custom_path)
        self.module = self.fetcher.build_backend(self.module_name, params={})

        # engine_config = "postgresql://{db_user}:{db_pass}@{ip}/{db_name}"
        # self.engine_config = ""
        self.dao_manager = DaoManager(self.module_name, self.engine_config)

        self.session, self.scoped_session = self.dao_manager.get_shared_session()

        self.timezone = pytz.timezone("Europe/Paris")

    def run(self):
        pass


if __name__ == "__main__":
    crawler = {{cookiecutter.camel_name}}Backend()
    crawler.run()
