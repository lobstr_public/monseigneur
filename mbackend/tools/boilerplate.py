from cookiecutter.main import cookiecutter
import os
import argparse


def set_templates_path():
    templates_path = '~/mdev/monseigneur/mbackend/tools/templates'
    return os.path.expanduser(templates_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group("Required arguments")
    required.add_argument('--module', '-m', required=True, help="Enter lowercase module name")
    required.add_argument('--camel', '-c', required=True, help="Enter camelcase name for module")
    parser.add_argument('--farma', '-f', action='store_true', help="Use this arg when setting up farma projects")
    args = parser.parse_args()

    templates_path = set_templates_path()

    template_path = os.path.join(templates_path, 'monseigneur_template')
    if args.farma:
        template_path = os.path.join(templates_path, 'farma_template')

    cookiecutter(
            template_path,
            no_input=True,
            extra_context={
                'directory_name': args.module,
                'camel_name': args.camel
                }
            )
