from sqlalchemy.orm import scoped_session


def session_decorator(func):
    def inner(self, *args, **kwargs):
        result = None
        session = self.Session()
        try:
            result = func(self, session, *args, **kwargs)
            session.commit()
        except:
            session.rollback()
            raise
        else:
            self.Session.remove()
            if result != None:
                return result
    return inner


def dbconnect(func):
    def inner(self, *args, **kwargs):
        ScopedSession = scoped_session(self.session_factory)
        result = None
        session = ScopedSession()
        try:
            result = func(self, session, *args, **kwargs)
            session.commit()
        except Exception as e:
            session.rollback()
            ScopedSession.remove()
            raise
        ScopedSession.remove()
        if result != None:
            return result
    return inner


def dbconnect_noexpire(func):
    def inner(self, *args, **kwargs):
        ScopedSession = scoped_session(self.session_factory)
        result = None
        session = ScopedSession()
        session.expire_on_commit = False
        try:
            result = func(self, session, *args, **kwargs)
            session.commit()
        except:
            session.rollback()
            raise
        else:
            ScopedSession.remove()
            if result != None:
                return result
    return inner


def dbconnect_sharedsession(func):
    def inner(self, *args, **kwargs):
        result = None
        shared_session = kwargs.pop('session')
        if shared_session is None:
            ScopedSession = scoped_session(self.session_factory)
            session = ScopedSession()
            session.expire_on_commit = False
        else:
            session = shared_session
            session.expire_on_commit = False
        try:
            result = func(self, session, *args, **kwargs)
            session.commit()
        except:
            session.rollback()
            raise
        else:
            if result != None:
                return result
    return inner


def dbconnect_noclosing(func):
    def inner(self, *args, **kwargs):
        ScopedSession = scoped_session(self.session_factory)
        result = None
        session = ScopedSession()
        session.expire_on_commit = False
        try:
            result = func(self, session, *args, **kwargs)
            session.commit()
        except:
            session.rollback()
            raise
        else:
            if result != None:
                return result, ScopedSession
            else:
                ScopedSession.remove()
                return None
    return inner


def dbconnect_noclosingandsession(func):
    def inner(self, *args, **kwargs):
        ScopedSession = scoped_session(self.session_factory)
        result = None
        session = ScopedSession()
        session.expire_on_commit = False
        try:
            result = func(self, session, *args, **kwargs)
            session.commit()
        except:
            session.rollback()
            raise
        else:
            if result != None:
                return result, session, ScopedSession
            else:
                ScopedSession.remove()
                raise
                return None
    return inner
