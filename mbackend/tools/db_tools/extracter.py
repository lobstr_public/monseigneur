from threading import Thread
from pytz import timezone

import MySQLdb
import csv
from sshtunnel import SSHTunnelForwarder

from monseigneur.core.tools.log import getLogger


class DbExtracter(Thread):
    def __init__(self,
                 db_name,
                 path,
                 extract_query,
                 fieldnames,
                 special_characters=['\n', '\r', '\t'],
                 ip=None,
                 username=None,
                 password=None,
                 sql_username=None,
                 sql_password=None):

        Thread.__init__(self)
        self._logger = getLogger("DbExtracter")
        self._logger.info('starting DbCleaner')

        self.ip = ip
        self.db_name = db_name
        self.username = username
        self.password = password
        self.sql_username = sql_username
        self.sql_password = sql_password

        self.path = path
        self.extract_query = extract_query
        self.special_characters = special_characters
        self.fieldnames = fieldnames

        self.tz = timezone('Europe/Paris')
        assert path
        assert db_name
        assert self.special_characters and isinstance(self.special_characters, list)

    def run(self):
        connection, cursor = self.get_connection_ssh()
        print(self.extract_query)
        cursor.execute(self.extract_query)
        data = cursor.fetchall()

        with open(self.path, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=self.fieldnames, delimiter='\t')
            writer.writeheader()
            for row in data:
                self._logger.info('now processing: {}'.format(row))
                row = list(row)

                for i in range(0, len(row), 1):
                    if row[i]:
                        if isinstance(row[i], str):
                            for special_character in self.special_characters:
                                row[i] = row[i].replace(special_character, '')
                row_dict = dict(zip(self.fieldnames, list(row)))
                writer.writerow(row_dict)

    def get_connection_ssh(self):
        if self.username and self.ip and self.password:
            server = SSHTunnelForwarder(
                ssh_address_or_host=self.ip,
                ssh_username=self.username,
                ssh_password=self.password,
                remote_bind_address=('127.0.0.1', 3306)
            )
            server.start()

            connection = MySQLdb.connect(
                host='127.0.0.1',
                user=self.sql_username,
                passwd=self.sql_password,
                port=server.local_bind_port,
                db=self.db_name,
                use_unicode=True,
                charset="utf8mb4"
            )

        else:

            connection = MySQLdb.connect(
                host='127.0.0.1',
                user=self.sql_username,
                passwd=self.sql_password,
                db=self.db_name,
                use_unicode=True,
                charset="utf8mb4"
            )

        cursor = connection.cursor()
        return connection, cursor


if __name__ == '__main__':
    db_name = 'your_db_name'
    path = 'path/to/save/file'
    extract_query = 'SELECT * FROM db.table;'
    fieldnames = ['list', 'of', 'fieldnames']
    ip = 'your.ip.'
    username = 'ssh_username'
    password = 'ssh_password'
    sql_username = 'sql_username'
    sql_password = 'sql_password'

    db_cleaner = DbExtracter(
        db_name=db_name,
        path=path,
        extract_query=extract_query,
        fieldnames=fieldnames,
        ip=ip,
        username=username,
        password=password,
        sql_username=sql_username,
        sql_password=sql_password)

    db_cleaner.start()
